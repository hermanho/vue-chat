import Vue from 'vue'
import axios from 'axios'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'
// import firebase from 'firebase'

import App from './App'
import router from './router'
import store from './store'
import VueTimeago from 'vue-timeago'

Vue.use(Vuetify)
if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(require('vue-moment'))

require('./assets/libs/idle')
require('./statusManager')

Vue.use(VueTimeago, {
  name: 'timeago', // component name, `timeago` by default
  locale: 'en-US',
  locales: {
    // you will need json-loader in webpack 1
    'en-US': require('vue-timeago/locales/en-US.json')
  }
})

let app
const config = {
  apiKey: 'AIzaSyDop6P-lm7Xt4hqYzC8QKZoX2xj2CHvnrg',
  authDomain: 'electron-app-1.firebaseapp.com',
  databaseURL: 'https://electron-app-1.firebaseio.com',
  projectId: 'electron-app-1',
  storageBucket: 'electron-app-1.appspot.com',
  messagingSenderId: '149101202407'
}

window.firebase.initializeApp(config)

window.firebase.auth().onAuthStateChanged((user) => {
  if (!app) {
    app =
      /* eslint-disable no-new */
      new Vue({
        components: { App },
        router,
        store,
        template: '<App/>'
      }).$mount('#app')
  }
})
