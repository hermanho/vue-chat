/* eslint-disable no-undef,no-unused-vars,new-parens */

// import firebase from 'firebase'

const awayCallback = function () {
  // console.log(new Date().toTimeString() + ': away')
  const user = firebase.auth().currentUser
  if (user) {
    firebase.database().ref('status').child(user.uid).set({status: 'away', date: (new Date).getTime()}).then(res => {})
  }
}

const awayBackCallback = function () {
  // console.log(new Date().toTimeString() + ': back')
  const user = firebase.auth().currentUser
  if (user) {
    firebase.database().ref('status').child(user.uid).set({status: 'online', date: (new Date).getTime()}).then(res => {})
  }
}
/* const onVisibleCallback = function () {
  // console.log(new Date().toTimeString() + ': now looking at page')
  const user = firebase.auth().currentUser
  if (user) {
    firebase.database().ref('status').child(user.uid).set('online').then(res => {})
  }
} */

const onHiddenCallback = function () {
  // console.log(new Date().toTimeString() + ': not looking at page')
  const user = firebase.auth().currentUser
  if (user) {
    firebase.database().ref('status').child(user.uid).set({status: 'offline', date: (new Date).getTime()}).then(res => {})
  }
}
// this is one way of using it.
/*
var idle = new Idle();
idle.onAway = awayCallback;
idle.onAwayBack = awayBackCallback;
idle.setAwayTimeout(2000);
idle.start();
*/
// this is another way of using it
const idle = new Idle({
  onHidden: onHiddenCallback,
  onVisible: awayBackCallback,
  onAway: awayCallback,
  onAwayBack: awayBackCallback,
  awayTimeout: 15000
}).start()
