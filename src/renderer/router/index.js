import Vue from 'vue'
import Router from 'vue-router'
// import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  mode: 'hash',
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'welcome-view',
      // component: require('@/components/WelcomeView').default
      component: require('@/components/views/Main').default,
      meta: {
        auth: true
      },
      children: [
        {
          path: '/chat/:me/:id',
          name: 'chat',
          component: require('@/components/views/ChatContainer').default
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: require('@/components/views/Auth').default
    },
    {
      path: '*',
      redirect: '/login'
    }
  ]
})

router.beforeEach((to, from, next) => {
  let currentUser = window.firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.auth)
  if (requiresAuth && !currentUser) {
    next('login')
  } else if (!requiresAuth && currentUser) {
    next('/')
  } else {
    next()
  }
})

export default router
