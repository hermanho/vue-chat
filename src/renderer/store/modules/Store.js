import Vue from 'vue'

const state = {
  user: {},
  friends: {},
  contacts: {},
  online: {}
}

const mutations = {
  SET_CURRENT_USER (state, user) {
    state.user = user
  },
  SET_FRIENDS (state, friends) {
    if (!friends) {
      state.friends = {}
    } else {
      if (friends.authKey) {
        Vue.set(state.friends, friends.authKey, friends)
      }
    }
  },
  ADD_CONTACT (state, c) {
    Vue.set(state.contacts, c.uid, c)
  },
  REMOVE_CONTACT (state, ct) {
    let newContacts = {}
    for (let c in state.contacts) {
      if (c !== ct.uid) {
        newContacts[ct.uid] = state.contacts[c]
      }
    }
    state.contacts = newContacts
  },
  RESET_STATE (state) {
    state.user = {}
    state.friends = {}
    state.contacts = {}
    state.online = {}
  },
  UPDATE_LAST_MESSAGE (state, data) {
    if (state.contacts[data.receiver]) {
      state.contacts[data.receiver].lastMessage = state.contacts[data.receiver].lastMessage || {}
      state.contacts[data.receiver].lastMessage[state.user.uid] = state.contacts[data.receiver].lastMessage[state.user.uid] || {}
      state.contacts[data.receiver].lastMessage[state.user.uid].content = data.message
      state.contacts[data.receiver].lastMessage[state.user.uid].sender = data.sender
      state.contacts[data.receiver].lastMessage[state.user.uid].date = data.date
    }
  },
  HANDLE_ONLINE_STATUS (state, data) {
    Vue.set(state.online, data.key, data.contact)
  }
}

const actions = {
  setUser ({ commit }, user) {
    commit('SET_CURRENT_USER', user)
  },
  addNewFriend ({ commit }, data) {
    commit('SET_FRIENDS', data)
  },
  addNewContact ({ commit }, data) {
    commit('ADD_CONTACT', data)
  },
  removeContact ({ commit }, data) {
    commit('REMOVE_CONTACT', data)
  },
  resetState ({ commit }) {
    commit('RESET_STATE')
  },
  updateLastMessage ({ commit }, data) {
    commit('UPDATE_LAST_MESSAGE', data)
  },
  handleOnlineStatus ({ commit }, data) {
    commit('HANDLE_ONLINE_STATUS', data)
  }
}

const getters = {
  getUser: state => state.user,
  getDefaultProfileImage: state => 'https://www.shareicon.net/download/2016/09/01/822711_user_512x512.png',
  getFriends: state => state.friends,
  getFriendRequests: state => {
    if (!state.friends || !state.user) {
      return null
    }
    const requests = []
    for (let r in state.friends) {
      let f = state.friends[r]
      if (f.uid !== state.user.uid) {
        requests.push(f)
      }
    }
    return requests
  },
  getMyContacts: state => {
    return state.contacts || {}
  },
  geOnlineStatuses: state => {
    return state.online || {}
  }
}

export default {
  state,
  mutations,
  actions,
  getters,
  strict: false
}
